package pa5;

public interface State {
	public void enterState();
	public void updatedTime();
	public void handleSetKey();
	public void handlePlusKey();
	public void handleMinusKey();
	public int getHour();
	public int getMinute();
	public int getSecond();
	public boolean Blink();
}

package pa5;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class CheapDigitalClockGUI extends JFrame{
	private ClassLoader loader;
	private JPanel mainPanel, timePanel, btnPanel;
	private JLabel hour1, hour2, colon1, min1, min2, colon2, sec1, sec2;
	private JButton setBtn, plusBtn, minBtn;
	private Clock clock;
	private ImageIcon[] pics;
	private Clip alarm;
	private boolean blink = true;

	public CheapDigitalClockGUI() {

		this.clock = new Clock();
		loader = this.getClass().getClassLoader();
		this.pics = new ImageIcon[12];
		for ( int i = 0 ; i<10 ; i++ ) {
			this.pics[i] = new ImageIcon(loader.getResource( "images/" + i + ".gif" ) );
		}
		this.pics[10] = new ImageIcon( loader.getResource( "images/colon.gif" ) );
		this.pics[11] = new ImageIcon( loader.getResource( "images/blank.gif" ) );
		URL loadSound = ClassLoader.getSystemResource( "sounds/alarm.wav" );
		try {
			AudioInputStream AIS = AudioSystem.getAudioInputStream( loadSound );
			alarm = AudioSystem.getClip();
			alarm.open( AIS );
		} catch (UnsupportedAudioFileException | IOException e) {
		} catch (LineUnavailableException e) {
		}
		this.setTitle("CheapDigitalClock");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(300, 150);
		initComponents();
	}

	public void initComponents() {

		this.mainPanel = new JPanel();
		this.mainPanel.setLayout( new BoxLayout( mainPanel, BoxLayout.Y_AXIS ) );

		this.timePanel = new JPanel();
		this.timePanel.setLayout( new BoxLayout( timePanel,  BoxLayout.X_AXIS ) );

		this.btnPanel = new JPanel();

		this.setBtn = new JButton( "SET" );
		this.plusBtn = new JButton( " + " );
		this.minBtn = new JButton( " - " );

		this.hour1 = new JLabel();
		this.hour1.setIcon(  pics[0]);
		this.hour2 = new JLabel();
		this.hour2.setIcon( pics[0] );
		this.colon1 = new JLabel();
		this.colon1.setIcon( pics[10] );
		this.min1 = new JLabel();
		this.min1.setIcon( pics[0] );
		this.min2 = new JLabel();
		this.min2.setIcon( pics[0] );
		this.colon2 = new JLabel();
		this.colon2.setIcon( pics[10] );
		this.sec1 = new JLabel();
		this.sec1.setIcon( pics[0] );
		this.sec2 = new JLabel();
		this.sec2.setIcon( pics[0] );

		this.timePanel.add( this.hour1 );
		this.timePanel.add( this.hour2 );
		this.timePanel.add( this.colon1 );
		this.timePanel.add( this.min1 );
		this.timePanel.add( this.min2 );
		this.timePanel.add( this.colon2 );
		this.timePanel.add( this.sec1 );
		this.timePanel.add( this.sec2 );

		this.setBtn.addActionListener( new setBtnListener() );
		this.plusBtn.addActionListener( new plusBtnListener() );
		this.minBtn.addActionListener( new minBtnListener() );

		this.btnPanel.add( setBtn );
		this.btnPanel.add( plusBtn );
		this.btnPanel.add( minBtn );

		this.mainPanel.add( this.timePanel );
		this.mainPanel.add( this.btnPanel );

		updateTime();
		mainPanel.setBackground(Color.BLACK);
		super.add( mainPanel );

	}

	public void updateTime() {
		int delay = 500;
		ActionListener task = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if ( clock.state == clock.RINGING ) {
					alarm.setFramePosition( 0 );
					alarm.loop( Clip.LOOP_CONTINUOUSLY );
				}
				
				if ( clock.state.Blink() ) {
					if ( clock.state == clock.SETHOUR ) {
						if ( blink ) {
							hour1.setIcon( pics[ clock.state.getHour()/10 ] );
							hour2.setIcon( pics[ clock.state.getHour()%10 ] );
							blink = false;
						} else {
							hour1.setIcon( pics[ 11 ] );
							hour2.setIcon( pics[ 11 ] );
							blink = true;
						}
					} else if ( clock.state == clock.SETMIN ) {
						if ( blink ) {
							min1.setIcon( pics[ clock.state.getMinute()/10 ] );
							min2.setIcon( pics[ clock.state.getMinute()%10 ] );
							blink = false;
						} else {
							min1.setIcon( pics[ 11 ] );
							min2.setIcon( pics[ 11 ] );
							blink = true;
						}
					} else if ( clock.state == clock.SETSEC ) {
						if ( blink ) {
							sec1.setIcon( pics[ clock.state.getSecond()/10 ] );
							sec2.setIcon( pics[ clock.state.getSecond()%10 ] );
							blink = false;
						} else {
							sec1.setIcon( pics[ 11 ] );
							sec2.setIcon( pics[ 11 ] );
							blink = true;
						}
					}
				} else {
					hour1.setIcon( pics[ clock.state.getHour()/10 ] );
					hour2.setIcon( pics[ clock.state.getHour()%10 ] );
					min1.setIcon( pics[ clock.state.getMinute()/10 ] );
					min2.setIcon( pics[ clock.state.getMinute()%10 ] );
					sec1.setIcon( pics[ clock.state.getSecond()/10 ] );
					sec2.setIcon( pics[ clock.state.getSecond()%10 ] );
				}
				clock.updatedTime();
			}
		};
		javax.swing.Timer timer = new javax.swing.Timer(delay, task);
		timer.start();
	}

	public void run() {

		this.setVisible(true);

	}

	class setBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			clock.state.handleSetKey();
		}
	}

	class plusBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			clock.state.handlePlusKey();
		}
	}

	class minBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			clock.state.handleMinusKey();
		}
	}

	public static void main (String[] args) {

		CheapDigitalClockGUI clock = new CheapDigitalClockGUI();
		clock.run();

	}


}

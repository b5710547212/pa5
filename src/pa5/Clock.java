package pa5;

import java.util.Calendar;

public class Clock {

	public State state;
	private Calendar time = Calendar.getInstance();
	private int hour = 0, min = 0, sec = 0;

	public Clock() {

		this.state = CLOCK;

	}

	public void set() {

		this.state.enterState();

	}

	public void updatedTime() { this.state.updatedTime(); }

	public void setState( State newState ) { this.state = newState; }

	State CLOCK = new State() {

		@Override
		public void enterState() {}

		@Override
		public void updatedTime() { 
			time.setTimeInMillis( System.currentTimeMillis() );
			if ( Calendar.HOUR_OF_DAY == hour && Calendar.MINUTE == min && Calendar.SECOND == sec ) { setState( RINGING ); }
		}

		@Override
		public void handleSetKey() { setState( SETHOUR ); }

		@Override
		public void handlePlusKey() { setState( PEAKTIME ); }

		@Override
		public void handleMinusKey() {}

		public int getHour() { return time.get( Calendar.HOUR_OF_DAY ); }

		public int getMinute() { return time.get( Calendar.MINUTE ); }

		public int getSecond() { return time.get( Calendar.SECOND ); }

		@Override
		public boolean Blink() { return false; }

	};

	State SETHOUR = new State() {

		@Override
		public void enterState() {}

		@Override
		public void updatedTime() {  if ( Calendar.HOUR_OF_DAY == hour && Calendar.MINUTE == min && Calendar.SECOND == sec ) { setState( RINGING ); }}

		@Override
		public void handleSetKey() { setState( SETMIN ); }

		@Override
		public void handlePlusKey() { hour = (hour+1)%24; }

		@Override
		public void handleMinusKey() { hour = (hour+23)%24; }

		public int getHour() { return hour; }

		public int getMinute() { return min; }

		public int getSecond() { return sec; }

		@Override
		public boolean Blink() { return true; }

	};

	State SETMIN = new State() {

		@Override
		public void enterState() {}

		@Override
		public void updatedTime() { if ( Calendar.HOUR_OF_DAY == hour && Calendar.MINUTE == min && Calendar.SECOND == sec ) { setState( RINGING ); } }

		@Override
		public void handleSetKey() { setState( SETSEC ); }

		@Override
		public void handlePlusKey() { min = (min+1)%60; }

		@Override
		public void handleMinusKey() { min = (min+59)%60; }

		public int getHour() { return hour; }

		public int getMinute() { return min; }

		public int getSecond() { return sec; }

		@Override
		public boolean Blink() { return true; }

	};

	State SETSEC = new State() {

		@Override
		public void enterState() {}

		@Override
		public void updatedTime() { if ( Calendar.HOUR_OF_DAY == hour && Calendar.MINUTE == min && Calendar.SECOND == sec ) { setState( RINGING ); } }

		@Override
		public void handleSetKey() { setState( CLOCK ); }

		@Override
		public void handlePlusKey() { sec = (sec+1)%60; }

		@Override
		public void handleMinusKey() { sec = (sec+59)%60; }

		public int getHour() { return hour; }

		public int getMinute() { return min; }

		public int getSecond() { return sec; }

		@Override
		public boolean Blink() { return true; }

	};

	State RINGING = new State() {

		@Override
		public void enterState() {}

		@Override
		public void updatedTime() { if ( Calendar.HOUR_OF_DAY == hour && Calendar.MINUTE == min && Calendar.SECOND == sec ) { setState( RINGING ); } }

		@Override
		public void handleSetKey() { setState( CLOCK ); }

		@Override
		public void handlePlusKey() { setState( CLOCK ); }

		@Override
		public void handleMinusKey() { setState( CLOCK ); }

		public int getHour() { return time.get( Calendar.HOUR_OF_DAY ); }

		public int getMinute() { return time.get( Calendar.MINUTE ); }

		public int getSecond() { return time.get( Calendar.SECOND ); }

		@Override
		public boolean Blink() { return true; }

	};

	State PEAKTIME = new State() {

		@Override
		public void enterState() {}

		@Override
		public void updatedTime() {}

		@Override
		public void handleSetKey() {}

		@Override
		public void handlePlusKey() { setState( CLOCK ); }

		@Override
		public void handleMinusKey() {}

		public int getHour() { return hour; }

		public int getMinute() { return min; }

		public int getSecond() { return sec; }

		@Override
		public boolean Blink() { return false; }

	};

}
